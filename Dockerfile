FROM golang:1.8.0-alpine

ADD . /go/src/nsq-event-to-http

RUN mkdir /data

ADD config.json /data

#Do a git clone of the boltzap-core.
RUN mkdir /root/.ssh/

# Copy over private key, and set permissions
ADD docker /root/.ssh/id_rsa

RUN chmod 600 /root/.ssh/id_rsa

# Create known_hosts
RUN touch /root/.ssh/known_hosts

RUN set -xe && \
    apk add --no-cache --virtual .build-deps git openssh && \
    ssh-keyscan gitlab.com >> /root/.ssh/known_hosts && cd /go/src && \
    git clone git@gitlab.com:boltzapteam/boltzap-core-api.git core && \
    cd /go/src/nsq-event-to-http && go get && go build && go install && \
    apk del .build-deps && rm -rf /root/.ssh

ENTRYPOINT ["/go/bin/nsq-event-to-http", "--config", "/data/config.json"]