package main

import (
	"core/config"
	"core/service"
	"github.com/nsqio/go-nsq"
    "github.com/parnurzeal/gorequest"
	"core/event"
	"encoding/json"
	"go.uber.org/zap"
	"errors"
	"strconv"
	"net/http"
	"time"
	"core"
)

const (
	//RequestKey the key in config to get request configuration.
	RequestKey = "request"
)

//RequestConfig configuration for each http request
type RequestConfig struct {
	RetryCount 	       int           `json:"retry_count"`
	Backoff 	       core.Duration `json:"backoff"`
	StatusRequirements []int         `json:"status_requirements"`
	Timeout 	       core.Duration `json:"timeout"`
	DefaultEndpoint    core.URL      `json:"default_endpoint"`
	DefaultMethod      core.Method   `json:"default_method"`
}

//NewRequestConfig generates request config with default values.
func NewRequestConfig() *RequestConfig {
	return &RequestConfig{
		RetryCount: 3,
		Backoff: core.Duration(2 * time.Second),
		StatusRequirements: []int{
			http.StatusBadRequest,
			http.StatusInternalServerError,
		},
		Timeout: core.Duration(4 * time.Second),
		DefaultMethod: core.Method(http.MethodPost),
	}
}

func main() {
	//Parse config from flag.
	cnf, err := config.ParseConfig()

	if err != nil {
		panic(err)
	}

	//Initialise consumer service.
	consumer, err := service.NewConsumerService()

	if err != nil {
		panic(err)
	}

	//set consumer variables from config.
	if err := cnf.Get(service.ConfigConsumer, consumer); err != nil {
		panic(err)
	}

	logger, err := zap.NewDevelopment()

	if err != nil {
		panic(err)
	}

	requestConfig := NewRequestConfig()

	if err := cnf.Get(RequestKey, requestConfig); err != nil {
		panic(err)
	}

	//set the handler.
	consumer.Handler = func(message *nsq.Message) error {
		logger.Info("received webhook event")
		wh := new(event.Webhook)
		if err := json.Unmarshal(message.Body, wh); err != nil {
			logger.Error("could not parse message data")
			return err
		}

		//set the event id to the NSQ message id.
		wh.EventID = string(message.ID[:])
		logger.Info("parsed webhook with id: " + wh.EventID)

		wh.RetryCount = int(message.Attempts)

		if wh.RetryCount > requestConfig.RetryCount {
			logger.Info("webhook has reached the retry attempt limit, skipping...")
			return nil //technically an error, but returning an error reschedules the message on nsq.
		}

		endpoint := wh.Endpoint.String()

		//check that we have a supplied endpoint and its valid.
		if endpoint == "" {
			dftEndpoint := requestConfig.DefaultEndpoint.String()
			if dftEndpoint == "" {
				//we don't have a valid endpoint or a valid fallback, return an error.
				return errors.New("we dont have a valid endpoint to post to.")
			}
			endpoint = dftEndpoint
		}

		method := wh.Method
		if !method.IsValid() {
			method = requestConfig.DefaultMethod
		}

		//post a request to the supplied endpoint with the data provided.
		logger.Info("sending a request to: " + endpoint)
		request := gorequest.New()
		resp, _, errs := request.CustomMethod(method.String(), endpoint).
			Set("X-Event-Request-ID", wh.EventID).
			Set("X-Event-Type", strconv.Itoa(wh.Type)).
			Set("X-Event-Retry-Count", strconv.Itoa(wh.RetryCount)).
			Send(wh.Event).
			Retry(
				requestConfig.RetryCount,
				time.Duration(requestConfig.Backoff),
				requestConfig.StatusRequirements...,
			).
			Timeout(time.Duration(requestConfig.Timeout)).
			End()

		if len(errs) > 0 {
			logger.Info("An error occurred processing the request.", zap.Any("errors", errs))
			return errors.New("an error occurred processing the request")
		}

		if resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusBadRequest {
			logger.Info("Request was a success")
			return nil
		}

		logger.Info("An error occurred processing the request, resceduling...")
		return errors.New("An error occurred processing the request")
	}

	if err := consumer.Consume(); err != nil {
		panic(err)
	}
}


