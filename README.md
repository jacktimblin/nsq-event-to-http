# README #

The Webhook rescheduler is used to consume and retry failed payment gateway web-hook events to add another layer of resilience to the platform. If a `Webhook` event is published to the `webhook` topic on NSQ then this consumer will retrieve that message and attempt the same request again up to 3 times. 

This service utilises the exponential backoff feature of NSQ to reduce the amount of time between failed requests on a payment gateways API.

The service deems a successful request as any request which returns a status code between 200 and 400, any other status code is deemed as an error and the request will be rescheduled.

##WebHook Events##

A webhook event consists of three main parts:

*  An endpoint to which to POST the request to.
*  Any JSON/XML body data to push with the request (in Stripe's case the Stripe Account ID and event ID)
*  An unique event ID (typically a UUID) which is pushed backed to the request handler in the `X-Request-Event-ID` header which can be used to tell the handler that the request is in-fact a retry.